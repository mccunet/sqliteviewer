package main

import (
	"database/sql"
	"fmt"
	"log"

	"github.com/lxn/walk"
	. "github.com/lxn/walk/declarative"

	_ "github.com/cznic/sqlite" // supports non-blocking parallel read access (no "datbase locked" errors)
)

type mainWindow struct {
	*walk.MainWindow
	dbpath           string
	db               *dbModel
	tvModel          *tvModel
	cbModel          *cbModel
	cbModelSuspended bool
	tv               *walk.TableView
	cb               *walk.ComboBox
	prevFilePath     string
	pb               *walk.PushButton
}

type dbModel struct {
	handle   *sql.DB
	tables   []string
	rowCount int
	cols     []string
}

func (mw *mainWindow) initMW() {
	// open the db
	mw.db = &dbModel{}
	var err error
	mw.db.handle, err = openDB(mw.dbpath)
	checkErr(err)
	mw.db.tables, err = getDBTables(mw.db.handle)
	checkErr(err)
	// populate the combobox
	mw.cbModel = newcbModel(mw.db.tables)
	mw.cbModelSuspended = true // don't call the update function when this is created
	// instantiate the table view
	mw.tvModel = &tvModel{}
}

func (mw *mainWindow) updateTable(table string) {
	// use the selected table to populate the table view
	var err error
	// Disable the TableView in case some clicks the "Update" button too quickly
	// We don't want to try to start another update before this one has finished.
	mw.clearTable()
	mw.db.cols, err = getDBColumns(mw.db.handle, table)
	mw.tvModel.cols = mw.db.cols
	mw.tvModel.ColumnList.Clear()
	for _, c := range mw.tvModel.cols {
		var newCol = new(walk.TableViewColumn)
		newCol.SetName(c)
		newCol.SetVisible(true)
		mw.tvModel.ColumnList.Add(newCol)
	}
	checkErr(err)
	mw.db.rowCount, err = getDBRowCount(mw.db.handle, table)
	checkErr(err)
	err = mw.addTracks(table)
	checkErr(err)
	// Notify TableView and other interested parties about the reset.
	mw.tvModel.PublishRowsReset()
	mw.tv.SetEnabled(true)
	mw.cbModelSuspended = false
}
func (mw *mainWindow) clearTable() {
	// If you have a large table and have scrolled down, when a smaller table is loaded,
	// there is an error generated for a bad index because the scroll index is beyond the last index of the shorter table.
	// Avoid this scenario by first clearing the table before loading the next table.
	var err error
	mw.tv.SetSuspended(true)
	mw.tvModel.ColumnList.Clear()
	mw.tv.SetEnabled(false)
	mw.tvModel.cols = []string{"a"}
	for _, c := range mw.tvModel.cols {
		var newCol = new(walk.TableViewColumn)
		newCol.SetName(c)
		newCol.SetVisible(true)
		mw.tvModel.ColumnList.Add(newCol)
	}
	checkErr(err)
	mw.clearTracks()
	// Notify TableView and other interested parties about the reset.
	mw.tvModel.PublishRowsReset()
	mw.tv.SetEnabled(true)
	mw.tv.SetSuspended(false)
	mw.cbModelSuspended = false
}

type cbModel struct {
	walk.ListModelBase
	items []string
}

func newcbModel(tables []string) *cbModel {
	m := &cbModel{items: make([]string, len(tables))}
	for i, t := range tables {
		m.items[i] = t
	}
	return m
}

// Must implement the ItemCount() and Value() methods!!
func (m *cbModel) ItemCount() int {
	return len(m.items)
}
func (m *cbModel) Value(index int) interface{} {
	return m.items[index]
}

type tvModel struct {
	walk.TableModelBase
	items      []map[string]interface{}
	cols       []string
	ColumnList *walk.TableViewColumnList
	dbRowCount int
}

// Must implement the RowCount() and Value() methods!!
func (m *tvModel) RowCount() int {
	return len(m.items)
}
func (m *tvModel) Value(row, col int) interface{} {
	item := m.items[row]
	return item[m.cols[col]]
}

func (mw *mainWindow) cbCurrentIndexChanged() {
	table := mw.cb.Text()
	fmt.Println("combobox index changed ", table)
	// It seems this event is published even as the ComboBox is being created
	// But when it fires this event while creating the ComboBox, the TableView may
	// not have been actually been created yet so this updateTable function will fail.
	// So the suspended boolean allows this handler to be blocked
	// until the UI has been completely created and is ready for use.
	if !mw.cbModelSuspended {
		mw.updateTable(table)
	}
}

func main() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered in f", r)
		}
	}()
	mw := &mainWindow{}
	//mw.dbpath = `.\data.sqlite`
	mw.dbpath = `.\results.sqlite`
	mw.initMW()
	//defer closeDB(mw.db.handle)
	var openAction *walk.Action
	MainWindow{
		AssignTo: &mw.MainWindow,
		Title:    "SQLiteViewer",
		MenuItems: []MenuItem{
			Menu{
				Text: "&File",
				Items: []MenuItem{
					Action{
						AssignTo:    &openAction,
						Text:        "&Open",
						OnTriggered: mw.openActionTriggered,
					},
					Separator{},
					Action{
						Text:        "Exit",
						OnTriggered: func() { mw.Close() },
					},
				},
			},
			Menu{
				Text: "&Help",
				Items: []MenuItem{
					Action{
						Text:        "About",
						OnTriggered: mw.aboutActionTriggered,
					},
				},
			},
		},
		Size:   Size{Width: 800, Height: 600},
		Layout: VBox{MarginsZero: true},
		Children: []Widget{
			Composite{
				Layout: VBox{MarginsZero: true},
				ContextMenuItems: []MenuItem{
					Action{
						Text: "E&xit",
						OnTriggered: func() {
							mw.Close()
						},
					},
				},
				Children: []Widget{
					Composite{
						Layout: HBox{MarginsZero: true},
						Children: []Widget{
							HSpacer{MinSize: Size{Width: 1}, MaxSize: Size{Width: 1}},
							Label{
								Text: "Table:",
							},
							ComboBox{
								AssignTo: &mw.cb,
								Model:    mw.cbModel,
								OnCurrentIndexChanged: mw.cbCurrentIndexChanged,
								MinSize:               Size{Width: 200},
							},
							PushButton{
								AssignTo: &mw.pb,
								Text:     "Update",
								OnClicked: func() {
									//mw.updateTable(mw.cb.Text())
									mw.clearTable()
								},
							},
							HSpacer{},
						},
					},
					TableView{
						AssignTo: &mw.tv,
						Columns:  mw.tvModel.setTVColumns(),
						Model:    mw.tvModel,
					},
				},
			},
		},
	}.Create()
	mw.cb.SetModel(mw.cbModel)
	mw.cb.SetCurrentIndex(0)
	icon, _ := walk.NewIconFromResourceId(10)
	mw.SetIcon(icon)
	// This line is pretty important.  It gives us access to the TableView's column list
	// which allows us to modify the TableView's columns.  This is done in the "mw.updateTable" method.
	mw.tvModel.ColumnList = mw.tv.Columns()
	mw.updateTable(mw.db.tables[0])
	mw.MainWindow.Run()
}
func openDB(dbPath string) (*sql.DB, error) {
	//dsn := fmt.Sprintf("file:%s?cache=shared&_busy_timeout=9999999", dbPath)
	dsn := fmt.Sprintf("file:%s?", dbPath)
	db, err := sql.Open("sqlite", dsn)
	//_, err = db.Exec("pragma journal_mode = WAL")
	return db, err
}
func closeDB(db *sql.DB) error {
	err := db.Close()
	return err
}
func getDBTables(db *sql.DB) ([]string, error) {
	var names = []string{}
	rows, err := db.Query("select name FROM 'main'.sqlite_master where type = 'table'")
	//rows, err := db.Query("select max(rowid) FROM testconditions")
	checkErr(err)
	for rows.Next() {
		var name string
		err = rows.Scan(&name)
		names = append(names, name)
		checkErr(err)
	}
	err = rows.Close()
	return names, err
}
func getDBRowCount(db *sql.DB, table string) (int, error) {
	rows, err := db.Query(fmt.Sprintf("select max(rowid) from %s", table))
	checkErr(err)
	var size int
	rows.Next()
	err = rows.Scan(&size)
	if size == 0 {
		err = nil
	} else {
		checkErr(err)
	}
	err = rows.Close()
	checkErr(err)
	return size, err
}
func getDBColumns(db *sql.DB, table string) ([]string, error) {
	rows, err := db.Query(fmt.Sprintf("select * from %s limit 1", table))
	checkErr(err)
	defer rows.Close()
	colNames, err := rows.Columns()
	checkErr(err)
	return colNames, err
}
func (mw *mainWindow) addTracks(table string) error {
	mw.tvModel.items = make([]map[string]interface{}, mw.db.rowCount)
	//table := mw.cb.Text()
	sqlStmt := fmt.Sprintf("select * from %s limit 10000", table)
	rows, err := mw.db.handle.Query(sqlStmt)
	if err != nil {
		return err
	}
	defer rows.Close()
	ifcCols := make([]interface{}, len(mw.db.cols))
	byteCols := make([][]byte, len(mw.db.cols))
	resultCols := make([]string, len(mw.db.cols))
	for i := range byteCols {
		// Have to scan into the interface{} type
		// so map the interface slice to the slice of bytes
		ifcCols[i] = &byteCols[i]
	}
	j := 0
	for rows.Next() {
		// The item interface slice will be a row in the Table View
		item := map[string]interface{}{}
		err := rows.Scan(ifcCols...)
		if err != nil {
			return err
		}
		for i := range ifcCols {
			resultCols[i] = string(byteCols[i])
			item[mw.db.cols[i]] = string(byteCols[i])
		}
		mw.tvModel.items[j] = item
		j++
	}
	err = rows.Close()
	checkErr(err)
	return nil
}
func (mw *mainWindow) clearTracks() error {
	mw.tvModel.items = make([]map[string]interface{}, 1)
	item := make(map[string]interface{}, 1)
	item["a"] = "1"
	mw.tvModel.items[0] = item
	return nil
}
func (m *tvModel) setTVColumns() []TableViewColumn {
	cls := []TableViewColumn{}
	for _, col := range m.cols {
		cls = append(cls, TableViewColumn{Name: col})
	}
	return cls
}
func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
func (mw *mainWindow) openActionTriggered() {
	mw.tv.SetItemStateChangedEventDelay(10)
	//mw.tvModel = &tvModel{}
	mw.db.handle.Close()
	dlg := new(walk.FileDialog)
	dlg.FilePath = mw.prevFilePath
	dlg.Filter = "SQLite Files (*.sqlite;*.sql3)|*.sqlite;*.sql3"
	dlg.Title = "Select a SQLite DB file"
	var err error
	if ok, err := dlg.ShowOpen(mw); err != nil {
		// return err
	} else if ok {
		mw.prevFilePath = dlg.FilePath
		mw.dbpath = dlg.FilePath
	}
	if err != nil {
		fmt.Printf("error closing previous db: %s", err)
	}
	checkErr(err)
	mw.db.handle, err = openDB(mw.dbpath)
	if err != nil {
		err = nil
		mw.db.handle, err = openDB(mw.dbpath)
	}
	checkErr(err)
	mw.db.tables, err = getDBTables(mw.db.handle)
	checkErr(err)
	mw.updateTable(mw.db.tables[0])
	// populate the combobox
	mw.cb.SetEnabled(false)
	mw.cbModelSuspended = true
	mw.cbModel = newcbModel(mw.db.tables)
	mw.cb.SetModel(mw.cbModel)
	mw.cb.SetCurrentIndex(0)
	mw.cbModelSuspended = false
	mw.cb.SetEnabled(true)
}
func (mw *mainWindow) aboutActionTriggered() {
	walk.MsgBox(mw, "About", "Walk Image Viewer Example", walk.MsgBoxIconInformation)
}
